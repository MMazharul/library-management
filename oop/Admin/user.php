<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 2/22/2018
 * Time: 9:13 PM
 */

namespace App\Admin;
use App\Model\Database;
use App\Utility\Utility;
use App\Message\Message;
use PDO;

class user extends Database
{
    public $id;
    public $name;
    public $email;
    public $password;
    public $cpassword;
    public $email_verified;


    public function setdata($data)
    {
        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data))
        {
            $this->password=md5($data['password']);
        }
        if(array_key_exists('cpassword',$data))
        {
            $this->cpassword=md5($data['cpassword']);
        }
        if(array_key_exists('email_token',$data))
        {
            $this->email_verified=$data['email_token'];
        }
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        return $this;
    }

    public function store(){

        $dataArray= array($this->name,$this->email,$this->password,$this->cpassword,$this->email_verified);

        $query="INSERT INTO `admin` (`name`, `email`, `password`, `cpassword`, `email_verified`) VALUES ( ?, ?, ?, ?, ?);";

        $STH=$this->conn->prepare($query);

        $result=$STH->execute($dataArray);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }



    public function change_password(){
        $query="UPDATE `library`.`admin` SET `password`=:password  WHERE `admin`.`email` =:email";
        $result=$this->conn->prepare($query);
        $check=$result->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($check){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }

    }

    public function view(){
        $query=" SELECT * FROM admin WHERE email = '$this->email' ";
        $STH =$this->conn->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }// end of view()
    public function singleinfo(){
        $query="SELECT * FROM admin WHERE id = '$this->id' ";
        $STH =$this->conn->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }


    public function validTokenUpdate(){
        $query="UPDATE `library`.`admin` SET  `email_verified`='".'Yes'."' WHERE `admin`.`email` ='$this->email'";
        $result=$this->conn->prepare($query);
        $check=$result->execute();

        if($check){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please user now!
              </div>");

        }
        else {
            echo "Error";
        }

    }

}