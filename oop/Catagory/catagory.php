<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 2/19/2018
 * Time: 10:57 AM
 */

namespace App\Catagory;
use App\Model\Database;
use PDO;
class catagory extends Database
{

    public $cat_id;
    public $catagory;

    public function catagorySet($data)
    {
        if(array_key_exists('catagory',$data))
        {
            $this->catagory=$data['catagory'];
        }
        if(array_key_exists('cat_id',$data))
        {
            $this->cat_id=$data['cat_id'];
        }
        return $this;
    }

    public function catagoryStore()
    {
        $catagory=array($this->catagory);
        $sql="INSERT INTO `catagory` (`catagory`) VALUES (?);";
        $prepare=$this->conn->prepare($sql);
        $data=$prepare->execute($catagory);
        return $data;
    }
    public function index()
    {
        $sql="SELECT * FROM `catagory`";
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;
    }
    public function catagoryEdit()
    {
        $sql="SELECT * FROM `catagory` WHERE cat_id = '$this->cat_id'";
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $editdata=$sth->fetch();
        return $editdata;
    }
    public function catagoryDelete()
    {
        $sql="DELETE FROM `catagory` WHERE `catagory`.`cat_id` ='$this->cat_id'";
        $delete=$this->conn->exec($sql);
        return $delete;
    }


}