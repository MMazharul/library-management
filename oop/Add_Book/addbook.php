<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 2/20/2018
 * Time: 4:46 AM
 */

namespace App\Add_Book;
use App\Model\Database;
use App\Utility\Utility;
use PDO;
class addbook extends Database
{
    public $id;
    public $cat_id;
    public $book;
    public $description;
    public $publisher;
    public $edition;
    public $catagory;
    public $page;
    public $image;
    public $e_book;



    public function addbookSet($data)
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('book',$data))
        {
            $this->book=$data['book'];
        }
        if(array_key_exists('description',$data))
        {
            $this->description=$data['description'];
        }
        if(array_key_exists('publisher',$data))
        {
            $this->publisher=$data['publisher'];
        }
        if(array_key_exists('edition',$data))
        {
            $this->edition=$data['edition'];
        }
        if(array_key_exists('catagory',$data))
        {
            $this->catagory=$data['catagory'];
        }
        if(array_key_exists('page',$data))
        {
            $this->page=$data['page'];
        }
        if(array_key_exists('image',$data))
        {
            $this->image=$data['image'];
        }
        if(array_key_exists('e_book',$data))
        {
            $this->e_book=$data['e_book'];
        }
        return $this;

    }


    public function addbooStore()
    {

        $addArray=array($this->book,$this->description,$this->publisher,$this->edition,$this->catagory,$this->page,$this->image,$this->e_book);
        $sql="INSERT INTO `addbook` (`book`, `description`, `publisher`,`edition`,`catagory`, `page`, `image`, `e_book`) VALUES (?,?,?,?,?,?,?,?);";
        $prepare=$this->conn->prepare($sql);
        $data=$prepare->execute($addArray);
        return $data;

    }
    public function catagory_index()
    {
        $sql="SELECT addbook.id,addbook.book,addbook.catagory,addbook.description,addbook.publisher,.addbook.edition,addbook.page,addbook.image,addbook.e_book FROM addbook INNER JOIN catagory ON addbook.catagory = catagory.catagory ";
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;
    }
    public function get_catagory()
    {
        $sql="SELECT * FROM `addbook` WHERE catagory = '$this->catagory'";
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
       return $alldata;
    }
    public function editBook()
    {
        $sql="SELECT * FROM `addbook` WHERE id = '$this->id'";
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetch();
        return $alldata;
    }
    public function view(){

        $sqlQuery = "Select * from addbook where id=".$this->id;

        $STH =$this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function bookDelete(){

        $sqlQuery = " DELETE FROM `addbook` WHERE `addbook`.`id` = '$this->id'";

        $delete=$STH =$this->conn->query($sqlQuery);

        return $delete;
    }
    public function bookUpdate()
    {
        $addArray=array($this->book,$this->description,$this->publisher,$this->edition,$this->page,$this->image,$this->e_book);
        $sql="UPDATE `addbook` SET `book` = ?, `description` = ?, `publisher` = ?, `edition`=?,`page` = ?, `image` = ?, `e_book` = ? WHERE `addbook`.`id` = '$this->id';";
        $prepare=$this->conn->prepare($sql);
        $data=$prepare->execute($addArray);
        return $data;
    }


}