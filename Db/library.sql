-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2018 at 02:45 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `addbook`
--

CREATE TABLE `addbook` (
  `id` int(11) NOT NULL,
  `book` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `publisher` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `edition` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `catagory` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `e_book` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addbook`
--

INSERT INTO `addbook` (`id`, `book`, `description`, `publisher`, `edition`, `catagory`, `page`, `image`, `e_book`) VALUES
(8, 'java', 'java is a smart language', 'saima mahmud', 'Janker Mahbub', 'cse', 118, '151936603127657080_2151219815108984_8339521337519031907_n.jpg', '1519540312975.pdf'),
(9, 'c', 'c is ', 'mazharul islam', '', '', 2, '151927560126112182_329488794225181_4378895643095141886_n.jpg', '1519275601ARRAY FUNCTIONS.pdf'),
(10, 'c', 'c is a good subject', 'harun rashi', 'Md Nasim', 'Programing', 241, '151930907626112240_329488887558505_4760139015228716315_n.jpg', ''),
(11, 'php', 'php is a server language,php er maddome_e server_er jabodio kaj korano hoi.....vibinno website er kaj php er maddom_e korano hoi\r\nphp is a pupular language in the world,,it is a smart language in other language', 'Mohi', 'Tahmid Rafi', 'Web', 461, '1519376149WIN_20171123_22_37_43_Pro.jpg', ''),
(12, 'business', 'business is a good subject,it is a very usefull of marketing site .', 'Alamin', 'Mazharul Islam', 'Management', 36, '1519376090WIN_20171122_00_47_08_Pro.jpg', ''),
(13, 'business process', 'business process is a good subject of managment', 'saima mahmood', 'saima mahmood', 'Management', 210, '151939731326112240_329488887558505_4760139015228716315_n.jpg', ''),
(14, 'c#', 'sdlfj dflru asldr sdlrkls dlrjdf', 'Sukkur', 'Tarequl Islam', 'Programing', 120, '15195405208.jpg', ''),
(15, 'c#', 'c# is a language', 'Sukkur Ali', 'Sukkur Ali', 'Cse', 10, '151966890628458880_2012353925704344_459942028_n.jpg', ''),
(16, 'Accounting Rules', 'lsdjflero dflkru selrjidruu', 'Sahed', 'Mahbubul Haque', 'Acounting', 125, '15195409861 (2).jpg', '1519540986Agile_Doc.docx');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cpassword` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `cpassword`, `email_verified`) VALUES
(8, 'Mazharul ', 'mazharulislam10000@gmail.com', '202cb962ac59075b964b07152d234b70', '25d55ad283aa400af464c76d713c07ad', 'Yes'),
(9, 'Saima Mahmood', 'cmtmazed89@gmail.com', '25d55ad283aa400af464c76d713c07ad', '25d55ad283aa400af464c76d713c07ad', 'Yes'),
(13, 'Mazed', 'mazed874989@gmail.com', '25d55ad283aa400af464c76d713c07ad', '25d55ad283aa400af464c76d713c07ad', '13b3679509be6b918cb5a06cf9e861b4');

-- --------------------------------------------------------

--
-- Table structure for table `catagory`
--

CREATE TABLE `catagory` (
  `cat_id` int(11) NOT NULL,
  `catagory` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `catagory`
--

INSERT INTO `catagory` (`cat_id`, `catagory`) VALUES
(15, 'Cse'),
(17, 'Programing'),
(21, 'Web'),
(22, 'Meghanical'),
(23, 'Power'),
(24, 'Agritecture'),
(25, 'Acounting'),
(26, 'Management');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cpassword` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `semister` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clg_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `cpassword`, `semister`, `clg_id`, `email_verified`) VALUES
(4, 'Mazharul ', 'mazharulislam10000@gmail.com', '202cb962ac59075b964b07152d234b70', '25d55ad283aa400af464c76d713c07ad', '4th Semister', '14', 'Yes'),
(5, 'Mazed', 'cmtmazed89@gmail.com', '25d55ad283aa400af464c76d713c07ad', '25d55ad283aa400af464c76d713c07ad', '3rd Semister', '14', 'Yes'),
(14, 'Mazharul ', 'mazed874989@gmail.com', '25d55ad283aa400af464c76d713c07ad', '25d55ad283aa400af464c76d713c07ad', '3rd Semister', '10', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addbook`
--
ALTER TABLE `addbook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catagory`
--
ALTER TABLE `catagory`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addbook`
--
ALTER TABLE `addbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `catagory`
--
ALTER TABLE `catagory`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
