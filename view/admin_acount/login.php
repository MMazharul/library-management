<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Online Login Form of library managment</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Online Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="../../Data/css/bootstrap/bootstrap.css" type="text/css"/> <!-- Style-CSS -->
<link rel="stylesheet" href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
<!-- //css files -->
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
<!-- //online-fonts -->
    <style>
        .login{margin-top:150px}
    </style>

</head>
<body>
			<div class="login col-md-4 col-md-offset-4">
                <h2>Administor Login</h2>
			<form action="../admin_authentication/login.php" method="post">
				<div class="form-group">
					<input placeholder="E-mail" class="form-control" name="email"  type="email" required="">
					<span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span>
				</div>
				<div class="pom-agile">
					<input  placeholder="Password" class="form-control" name="password"  type="password" required="">
					<span class="icon2"><i class="fa fa-unlock" aria-hidden="true"></i></span>
				</div>
				<div class="sub-w3l">
					<h6><a href="forgotten.php">Forgot Password?</a></h6>
					<div class="right-w3l">
                        <button type="submit" class="btn btn-primary"><span class="fa fa-key"></span> Login</button>
					</div>
				</div>
			</form>
		</div>

</body>
</html>