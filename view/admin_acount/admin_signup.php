<?php
require '../../vendor/autoload.php';
use App\Message\Message;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>


    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../index.php">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Gallery</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="fa fa-user"></span> Sign Up</a></li>
                <li><a href="#"><span class="fa fa-key"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="m"><?php echo Message::message()?></div>
<h2 align="center">Admin Registation Form</h2>
<div class="col-sm-4 col-sm-offset-4">
    <form action="admin_registration.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="user">AdminName</label>
            <input type="text" class="form-control" name="name" id="user" aria-describedby="emailHelp" placeholder="UserName">
        </div>
        <div class="form-group">
            <label for="Email1">Email address</label>
            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="Password1">Password</label>
            <input type="password" class="form-control" name="password" id="Password1" placeholder="Password">
        </div>
        <div class="form-group">
            <label for="Password1">Confirm Password</label>
            <input type="password" class="form-control" name="cpassword" id="Password1" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
<script>
    $('.m').slideDown("slow").delay(5000).slideUp("slow");
</script>
</html>