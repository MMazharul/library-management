
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>


    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">

    <style>
        h2{
            padding:10px 46px
        }
        p {
            font-size: 20px;
            padding:0 50px;
        }
        #map{
            height:250px;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../index.php">Home</a></li>
                <li><a href="catagory_index.php">BookCatagory</a></li>
                <li><a href="#">Conduct</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../Studentacount/SignUp.php"><span class="fa fa-user"></span> SignUp </a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Log In
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../Studentacount/login.php"><span class="fa fa-key"></span> Student Log In</a></li>
                        <li><a href="../admin_acount/login.php"><span class="fa fa-key"></span> Admin Log In</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<section id="conduct">
    <div class="container-fluid">
        <div class="row">
           <div class="col-sm-12">
               <h2><span class="fa fa-phone-square">  Contact Us</span></h2>
                    <p>International Islamic University Chittagong
                    Kumira, Chittagong-4318, Bangladesh<br>
                    Tel.: +88-03042-51154-61
                    Fax.: 03042 51160<br>
                    Email: info@iiuc.ac.bd</p>
           </div>
        </div>
    </div>
</section>
<section id="google">
    <div class="container-fluid">
        <div class="col-md-12">
            <h3 class="text-center">Google Map</h3>
            <div id="map">
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="../../Data/js/gmap3.min.js"></script>
<script src="../../Data/js/gmap.js"></script>

</body>
</html>