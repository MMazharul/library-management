<?php
require '../../vendor/autoload.php';
use App\Utility\Utility;
use App\Add_Book\addbook;
$catagory=new addbook();
$check=$catagory->addbookSet($_GET);
$all=$catagory->get_catagory();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/w3.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">

    <style>
        table {
            margin-top:60px
        }
    </style>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../index.php">Home</a></li>
                <li><a href="catagory_index.php">BookCatagory</a></li>
                <li><a href="conduct.php">Conduct</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Log Out
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="col-sm-8 col-sm-offset-2">
    <table class='w3-table w3-striped w3-bordered w3-card-4'>
        <tr class="w3-black">
            <th>BookName</th>
            <th>BookDescription</th>
            <th>Publisher</th>
            <th>Edition</th>
            <th>page</th>
            <th>e_book</th>
            <th>View</th>

        </tr>


<?php
foreach ($all as $value)
{
    echo "
    
        
       <tr>
        <td>$value->book</td>
        <td>$value->description </td>
        <td>$value->publisher</td>
        <td>$value->edition</td>
        <td>$value->page</td>
 
        <td><a href='../Category/e_book/$value->e_book'><img src='../Category/images/$value->image' height='100px' width='100px'/></a></td>
        <td><a class='w3-btn w3-blue' href='singlebook_info.php?id=$value->id'><span class='fa fa-info-circle'> view</a></td>
      </tr> 
    "
    ;
}
?>
    </table>
</div>
</body>
</html>

