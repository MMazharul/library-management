<?php
if(!isset($_SESSION) )session_start();
require '../../vendor/autoload.php';
use App\Utility\Utility;
use App\Add_Book\addbook;
use App\Catagory\catagory;
use App\Login\auth;
use App\Login\user;
use App\Message\Message;
$addbook=new addbook();
$id=$addbook->editBook();

$catagory=new catagory();
$all=$catagory->index();


$obj=new user();
$obj->setData($_SESSION);
$singleuser=$obj->view();

$auth=new auth();
$status=$auth->setData($_SESSION)->logged_in();
if(!$status) {
    Message::message('<h2 align="center"><strong>SorrY</strong> you are not logIn..Please logIn!!!!!!</h2>');
    return Utility::redirect('../Studentacount/SignUp.php');

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>

    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/w3.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
    <style>
        .col-sm-4{
            padding-top:50px;
        }
        .col-sm-4 table tr th{
            border-top:none;
        }
        .col-sm-4 table tr td a{
            text-decoration: none
        }

    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../index.php">Home</a></li>
                <li><a href="catagory_index.php">BookCatagory</a></li>
                <li><a href="conduct.php">Conduct</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Log Out
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>

                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="col-sm-4 col-sm-offset-4">
<table class='w3-table w3-striped w3-bordered w3-card-4'>
    <tr>
        <th class="w3-blue">Catagory List</th>
    </tr>

<?php

foreach ($all as $value)
{
    echo "
       
    <tr>
       
      <td><a href='catagory_view.php?catagory=$value->catagory'>$value->catagory</a></td>
    </tr>
   

    "
    ;
}
?>
</table>
</div>
</body>
</html>

