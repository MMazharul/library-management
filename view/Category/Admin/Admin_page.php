<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\Admin\auth;
use App\Admin\user;
use App\Message\Message;
use App\Utility\Utility;
$obj=new user();
$obj->setData($_SESSION);
$singleuser=$obj->view();

$auth=new auth();
$status=$auth->setData($_SESSION)->logged_in();
if(!$status) {
    Message::message('<h2 align="center"><strong>Sorry</strong> you are not logIn!!</h2>');
    return Utility::redirect('../../admin_acount/login.php');

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="../../../Data/js/jquery.min.js"></script>
    <script src="../../../Data/js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
    <link href="../../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../../Data/css/custom.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../../Studentacount/SignUp.php">Home</a></li>
                <li><a href="../catagory_index.php">Catagory</a></li>
                <li><a href="../addbook_index.php">All Catagory Books</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Admin
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../../admin_authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>