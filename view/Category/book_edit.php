<?php
require '../../vendor/autoload.php';
use App\Utility\Utility;
use App\Add_Book\addbook;


$catagory=new addbook();
$catagory->addbookSet($_GET);
$edit=$catagory->editBook();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>
    <link href=".././../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
</head>
<body>
<h2 align="center">Add_Book</h2>
<div class="col-sm-4 col-sm-offset-4">
    <form action="bookUpdate.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Book_Name</label>
            <input type="text" class="form-control" value="<?php echo $edit->book ?>" id="PUBLISHER" name="book" aria-describedby="emailHelp" placeholder="Book_Name">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea cols="5" class="form-control"  rows="5" name="description" id="description"><?php echo $edit->description ?></textarea>
        </div>
        <div class="form-group">
            <label for="PUBLISHER">PUBLISHER</label>
            <input type="text" class="form-control"  value="<?php echo $edit->publisher ?>" id="PUBLISHER" name="publisher" aria-describedby="emailHelp" placeholder="PUBLISHER">
        </div>
        <div class="form-group">
            <label for="edition">Edition</label>
            <input type="text" class="form-control"  value="<?php echo $edit->edition ?>" id="edition" name="edition" aria-describedby="emailHelp" placeholder="Edition">
        </div>
        <div class="form-group">
            <label for="Page">Page</label>
            <input type="number" class="form-control" value="<?php echo $edit->page ?>" name="page" id="Page" aria-describedby="emailHelp" placeholder="page">
        </div>
        <div class="form-group">
            <label for="image">image</label>
            <input type="file" name="image" id="image">
            <img src="images/<?php echo $edit->image;?>" width="100px" height="50px">
        </div>
        <div class="form-group">
            <label for="ebook">E-Book</label>
            <input type="file" value="<?php echo $edit->e_book ?>"  name="e_book" id="ebook">
            <a href="e_book/<?php echo $edit->e_book;?>"><img src="images/<?php echo $edit->image;?>" width="100px" height="50px"></a>
        </div>
        <input type="hidden" value="<?php echo $edit->id?>" name="id">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>
