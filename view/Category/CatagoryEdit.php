<?php
require '../../vendor/autoload.php';
use App\Catagory\catagory;
use App\Utility\Utility;

$object=new catagory();
$object->catagorySet($_GET);
$Edit=$object->catagoryEdit();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Catagory</title>
    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>
    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
    <style>
        .col-sm-4{margin-top:30px}
    </style>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="Admin/Admin_page.php">Home</a></li>
                <li><a href="catagory_index.php">Catagory</a></li>
                <li><a href="addbook_index.php">Book</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Admin
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../admin_authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="col-sm-4 col-sm-offset-4">
    <form action="catagoryupdate.php" method="post">
        <div class="form-group">
            <label for="name">CatagoryEdit</label>
            <input type="text" class="form-control" id="name" value="<?php echo $Edit->catagory ?>" name="catagory" aria-describedby="emailHelp" placeholder="Catagory">
            <input type="hidden" value="<?php echo $Edit->catagory?> " name="catagory_id">
        </div>
        <button type="submit" class="btn btn-primary"><span class='fa fa-plus-circle'>Update</button>
    </form>
</div>
</body>
</html>
