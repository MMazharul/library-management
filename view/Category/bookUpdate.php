<?php
require '../../vendor/autoload.php';
use App\Add_Book\addbook;
use App\Utility\Utility;

$profile=new addbook();

$profile->addbookSet($_POST);
$singleData = $profile->view();

$fileName = $singleData->image;

if( $_FILES['image']['name']!="" ){

    $fileName = time(). $_FILES['image'] ['name'];

    $source = $_FILES['image'] ['tmp_name'];

    $destination= "images/".$fileName;

    move_uploaded_file($source,$destination);
}
$_POST['image'] =$fileName;
$document = $singleData->e_book;
if($_FILES['e_book']['name']!="" )
{
    $ebook = time(). $_FILES['e_book'] ['name'];

    $source = $_FILES['e_book'] ['tmp_name'];

    $destination= "e_book/".$ebook;

    move_uploaded_file($source,$destination);
}

$_POST['e_book'] =$ebook;
$check=$profile->addbookSet($_POST);

$profile->bookUpdate();
Utility::redirect("addbook_index.php");




