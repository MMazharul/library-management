<?php
require'../../vendor/autoload.php';
use App\Add_Book\addbook;
use App\Utility\Utility;

$addbook=new addbook();

$filename = time() . $_FILES['image'] ['name'];
$source = $_FILES['image'] ['tmp_name'];
$destination = "images/" . $filename;
move_uploaded_file($source, $destination);
$_POST['image'] = $filename;

$e_book = time() . $_FILES['e_book'] ['name'];
$source = $_FILES['e_book'] ['tmp_name'];
$destination = "e_book/" . $e_book;
move_uploaded_file($source, $destination);
$_POST['e_book'] = $e_book;
$addbook->addbookSet($_POST);

$send=$addbook->addbooStore();
if($send)
{
    Utility::redirect('add_book.php');
}
else{
    echo "data is not send";
}

