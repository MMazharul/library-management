<?php
require '../../vendor/autoload.php';
use App\Utility\Utility;
use App\Catagory\catagory;
$catagory=new catagory();
$all=$catagory->index();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>

    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="Admin/Admin_page.php">Home</a></li>
                <li><a href="catagory_index.php">Catagory</a></li>
                <li><a href="addbook_index.php">Book</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Admin
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../admin_authentication/logout.php"><span class="fa fa-lock"></span> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<h2 align="center">Add_Book</h2>
<div class="col-sm-4 col-sm-offset-4">
    <form action="addbook_store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Book_Name</label>
            <input type="text" class="form-control" id="PUBLISHER" name="book" aria-describedby="emailHelp" placeholder="Book_Name">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea cols="5" class="form-control" rows="5" name="description" id="description"></textarea>
        </div>
        <div class="form-group">
            <label for="PUBLISHER">PUBLISHER</label>
            <input type="text" class="form-control" id="PUBLISHER" name="publisher" aria-describedby="emailHelp" placeholder="PUBLISHER">
        </div>
        <div class="form-group">
            <label for="edition">Edition</label>
            <input type="text" class="form-control" id="edition" name="edition" aria-describedby="emailHelp" placeholder="Edition">
        </div>
        <div class="form-group">
            <label for="sell">add catagory</label>
            <select class="form-control" id="sel1" name="catagory">
               <?php
               foreach ($all as $value)
               {
                   echo "
                 <option value='$value->catagory'>$value->catagory</option>
               ";
               }
               ?>
            </select>
        </div>
        <div class="form-group">
            <label for="Page">Page</label>
            <input type="number" class="form-control" name="page" id="Page" aria-describedby="emailHelp" placeholder="page">
        </div>
        <div class="form-group">
            <label for="image">image</label>
            <input type="file"  name="image" id="image">
        </div>
        <div class="form-group">
            <label for="ebook">E-Book</label>
            <input type="file"  name="e_book" id="ebook">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>