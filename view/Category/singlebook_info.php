<?php
require '../../vendor/autoload.php';

$catagory=new \App\Add_Book\addbook();
$catagory->addbookSet($_GET);
$singleinfo=$catagory->editBook();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>
    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
    <style>
        body{
            background:white;
            color:black;
            font-weight:bold;
        }
        h2{
            padding-bottom:20px
        }
        .info{
            margin-top:50px;
        }
        img{
            border:5px solid whitesmoke;
            box-shadow:0 0 5px 0 black;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="../index.php">Home</a></li>
                <li><a href="catagory_index.php">Catagory</a></li>
                <li><a href="#">Book</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Admin
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../admin_authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="main col-md-12 col-sm-12 col-xs-12">
<div class="info col-sm-4 col-sm-offset-4">
    <h2 class="text-center">Book Details</h2>
    <div class="col-sm-6">
        <?php echo "
        <a href='e_book/$singleinfo->e_book'><img src='images/$singleinfo->image' width='150px' height='150px'></a>
        ";?>
    </div>
    <div class="col-sm-6">
    <table class="table">

        <?php
            echo
            "
            <tr><td>BookName    :$singleinfo->book</td></tr>
            <tr><td>Publisher   :$singleinfo->publisher</td></tr>
            <tr><td>Publisher   :$singleinfo->edition</td></tr>
            <tr><td>Page        :$singleinfo->page</td></tr>
            ";
        ?>
    </table>
    </div>
</div>
<div class="d col-sm-4 col-md-offset-4">
    <h2 class="text-center page-header">Book Description</h2>
    <?php echo $singleinfo->description?>
</div>
</div>
</body>
</html>
