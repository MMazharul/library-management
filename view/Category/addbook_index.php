<?php
require '../../vendor/autoload.php';
use App\Utility\Utility;
use App\Add_Book\addbook;
$catagory=new addbook();
$all=$catagory->catagory_index();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>
    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/w3.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">

    <style>
        h2{margin-bottom:30px}
        .index{margin-bottom:100px}
    </style>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="Admin/Admin_page.php">Home</a></li>
                <li><a href="catagory_index.php">Catagory</a></li>
                <li><a href="add_book.php">add Book</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Admin
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../admin_authentication/logout.php"><span class="fa fa-lock"></span>  Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="index col-md-10 col-md-offset-1">
        <h2 class="text-center ">All Catagory Books</h2>
        <table class="w3-table w3-striped w3-bordered w3-card-4">
            <tr class="w3-blue-grey w3-animate-top">
                <th>BookName</th>
                <th>Description</th>
                <th>Catagory</th>
                <th>Publisher</th>
                <th>Edition</th>
                <th>page</th>
                <th>e_book</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>View</th>
            </tr>

<?php
foreach ($all as $value)
{
    echo "
    
        <div class''>
       <tr>
           
            <td>$value->book</td>
            <td>$value->description</td>
            <td>$value->catagory</td>
            <td>$value->publisher</td>
            <td>$value->edition</td>
            <td>$value->page</td>
           
            <td><a href='e_book/$value->e_book'><img src='images/$value->image' width='100px' height='40px'/></a></td>
            <td><a class='w3-btn w3-green' href='book_edit.php?id=$value->id'><span class='fa fa-edit'></span> Edit</a></td>
            <td><a class='w3-btn w3-red' href='bookDelete.php?id=$value->id'><span class='fa fa-remove'> Delete</a></td>
            <td><a class='w3-btn w3-blue' href='singlebook_info.php?id=$value->id'><span class='fa fa-info-circle'> View</a></td>
        </tr>
        </div>
    "
    ;
}
?>
        </table>
    </div>
</div>
</body>
</html>

