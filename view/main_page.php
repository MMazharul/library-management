<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\Login\auth;
use App\Login\user;
use App\Message\Message;
use App\Utility\Utility;
$obj=new user();
$obj->setData($_SESSION);
$singleuser=$obj->view();

$auth=new auth();
$status=$auth->setData($_SESSION)->logged_in();
if(!$status) {
    Message::message('<h2 align="center"><strong>Sorry</strong> you are not logIn!!</h2>');
    return Utility::redirect('Studentacount/SignUp.php');

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Library Management</title>

    <!--bootstrap jaavascript-->
    <script src="../Data/js/jquery.min.js"></script>
    <script src="../Data/js/bootstrap.js"></script>

    <!--google font-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
    <!--bootstrap css-->
    <link href="../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <!--font awsome-->
    <link href="../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <!--custom css-->
    <link href="../Data/css/custom.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="Studentacount/SignUp.php">Home</a></li>
                <li><a href="other/catagory_index.php">Books</a></li>
                <li><a href="other/conduct.php">Conduct</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="fa fa-user"></span> SignUp </a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Sign In
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="authentication/logout.php"><span class="fa fa-lock"></span> log Out<?php echo " ($singleuser->name)"; ?></a></li>
                        <li><a href="#"><span class="fa fa-key"></span> Admin Sign In</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<section id="baner">
    <div class="container-fluid">
        <div class="row">
        <!--this class background image set-->
        </div>
    </div>
</section>

<section id="about">
    <div class="container-fluid">
        <div class="row thumbnail">
            <h2>IIUC Central Library</h2>
                <div class="col-xm-12 col-sm-4 col-md-4 s">
                   <blockquote> <img src="../Data/image/central-library.jpg" class="img-responsive"/></blockquote>
                </div>
                <div class="col-xm-12 col-sm-4 col-md-4">
                    <blockquote>Library is the heart of an institution. International Islamic University Chittagong (IIUC) authority has established the University library in 1995 at the beginning of its journey. Now it becomes the largest Library among all the private university libraries of the country. Its full name is “Library and Information Division (LID)”.

                        There are over than 1, 44,759 books in IIUC Library covering the subjects relating to the academic programmers and other co-curriculum disciplines. There are also journals, periodicals, research and thesis papers, reports, conference proceedings, handbooks, manuals, encyclopedias, dictionaries, CD, VCD etc. for the users which are treated as reference collections and these can only be used within the library.
                    </blockquote>

                </div>
                <div class="col-xm-12 col-sm-4 col-md-4">

                    <blockquote>
                        Objectives
                        <ol itemtype="circle">
                            <li>Develop and maintain an understanding of the library’s current and potential users and their needs, and to respond to them appropriately</li>
                            <li>Develop information resources and provide easy access to them</li>
                            <li>Develop human resources of the library to ensure delivery of quality services and to maintain high levels of professionalism</li>
                            <li>Develop and implement a systematic approach to market the Library and its services</li>
                            <li>Exploit the potential of technology in all areas to deliver a wide spectrum of excellent information services.</li>
                    </blockquote>
                </div>
        </div>
    </div>
</section>
<section id="footer">
    <div class="container-fluid">
        <div class="col-md-12">
            <p>&copy;IIUC Library 2018</p>
        </div>

    </div>
</section>
</body>
</html>