<?php
require '../../vendor/autoload.php';
use App\Message\Message;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="../../Data/js/jquery.min.js"></script>
    <script src="../../Data/js/bootstrap.js"></script>


    <link href="../../Data/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../Data/css/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../../Data/css/custom.css" rel="stylesheet">
</head>
<body>
<div class="m col-sm-4 col-sm-offset-4"><?php echo Message::message()?></div>
<div class="col-sm-4 col-sm-offset-4">
    <h2 align="center">Registation Form</h2>
        <form action="registration.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="user">UserName</label>
                <input type="text" class="form-control" name="name" id="user" aria-describedby="emailHelp" placeholder="UserName" required>
            </div>
            <div class="form-group">
                <label for="Email1">Email address</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <label for="Password1">Password</label>
                <input type="password" class="form-control" name="password" id="Password1" placeholder="Password" required>
            </div>
            <div class="form-group">
                <label for="Password1">Confirm Password</label>
                <input type="password" class="form-control" name="cpassword" id="Password1" placeholder="Password" required>
            </div>
            <div class="form-group">
                <label for="sel1">Semister</label>
                <select class="form-control" id="sel1" name="semister" required>
                    <option value="1st Semister">1st Semister</option>
                    <option value="2nd Semister">2nd Semister</option>
                    <option value="3rd Semister">3rd Semister</option>
                    <option value="4th Semister">4th Semister</option>
                    <option value="5th Semister">5th Semister</option>
                    <option value="6th Semister">6th Semister</option>
                    <option value="7th Semister">7th Semister</option>
                </select>
            </div>
            <div class="form-group">
                <label for="id">ID</label>
                <input type="number" class="form-control" name="clg_id" id="id" aria-describedby="emailHelp" placeholder="ID" required>
            </div>
            <p>Already Any Acount?<a href="login.php"> SignIn</a></p>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
<script>
    $('.m').slideDown("slow").delay(5000).slideUp("slow");
</script>
</html>